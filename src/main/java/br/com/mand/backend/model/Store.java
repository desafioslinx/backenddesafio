package br.com.mand.backend.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Table
@Entity
@Data
public class Store implements Serializable{

	private static final long serialVersionUID = -3395564694265130132L;

	public final static Integer PROFIT_MARGIN = 10;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String name;
	
	private Double totalExpenses;
}
