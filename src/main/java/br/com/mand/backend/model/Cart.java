package br.com.mand.backend.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Table
@Entity
@Data
public class Cart implements Serializable{

	private static final long serialVersionUID = 6258253313774224482L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private Double totalPrice;
	
	@OneToMany(mappedBy = "cart")
	@JsonManagedReference
	private List<ProductItem> productItens;
	
}
