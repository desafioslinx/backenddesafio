package br.com.mand.backend.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Table
@Entity
@Data
public class ProductItem {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "product_fk"),name = "id_product")
	private Product product;
	
	private Integer quantidade;
	
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "cart_fk"),name = "id_cart")
	@JsonBackReference
	private Cart cart;
	
}
