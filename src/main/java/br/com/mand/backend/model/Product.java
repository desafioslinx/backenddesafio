package br.com.mand.backend.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Table
@Entity
@Data
public class Product implements Serializable {

	private static final long serialVersionUID = -457679297584795530L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String name;
	
	private String description;
	
	private String urlImage;
	
	private Double price;
	
	private Double purchaseCost;
	
	private Double expensesApportionment;
	
}