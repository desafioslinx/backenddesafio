package br.com.mand.backend.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mand.backend.dao.ProductRepository;
import br.com.mand.backend.dto.ProductDTO;
import br.com.mand.backend.model.Store;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	public List<ProductDTO> getAllProducts() {
		List<ProductDTO> newPage = productRepository.findAll().stream().map(tes -> {
			double priceCalculed = (tes.getPurchaseCost() + tes.getExpensesApportionment()) * (1 + Store.PROFIT_MARGIN/100.0);
			ProductDTO dto = new ProductDTO(tes.getId(),tes.getName(), tes.getDescription(), priceCalculed, tes.getUrlImage());
			return dto;
		}).collect(Collectors.toList());
		return newPage;
 	}

}
