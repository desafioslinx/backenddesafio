package br.com.mand.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mand.backend.dao.CartRepository;
import br.com.mand.backend.dao.ProductItemRepository;
import br.com.mand.backend.model.Cart;
import br.com.mand.backend.model.ProductItem;
import br.com.mand.backend.model.Store;

@Service
public class ProductItemService {

	@Autowired
	private ProductItemRepository itemRepository;

	@Autowired
	private CartRepository cartRepository;

	public ProductItem create(ProductItem itemProduct) {
		if (itemProduct.getCart() == null) {
			createAndAssingCartToItem(itemProduct);
		}
		else {
			itemProduct = fillProductPriceAndCartPrice(itemProduct);
		}
		return itemRepository.save(itemProduct);
	}
	
	private void createAndAssingCartToItem(ProductItem itemProduct) {
		Cart cart = new Cart();
		cart.setTotalPrice(itemProduct.getCart() != null ? itemProduct.getCart().getTotalPrice() : 0 + itemProduct.getQuantidade() * itemProduct.getProduct().getPrice());
		cartRepository.save(cart);
		itemProduct.setCart(cart);		
	}
	
	private ProductItem fillProductPriceAndCartPrice(ProductItem itemProduct) {
		
		ProductItem item = itemRepository.existsByProductIdAndCartId(itemProduct.getProduct().getId(),itemProduct.getCart().getId());
		itemProduct =  item != null ? item : itemProduct ;
		itemProduct.setQuantidade(item != null ? item.getQuantidade()+1 : itemProduct.getQuantidade());
		if (itemProduct.getProduct().getExpensesApportionment() != null) {			
			itemProduct.getProduct().setPrice((itemProduct.getProduct().getPurchaseCost() + itemProduct.getProduct().getExpensesApportionment()) * (1 + Store.PROFIT_MARGIN/100.0));
		}
		itemProduct.getCart().setTotalPrice(itemProduct.getQuantidade() * itemProduct.getProduct().getPrice());
		return itemProduct;
	}

}
