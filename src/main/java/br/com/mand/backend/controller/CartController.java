package br.com.mand.backend.controller;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.mand.backend.dao.CartRepository;
import br.com.mand.backend.model.Cart;
import br.com.mand.backend.model.ProductItem;
import br.com.mand.backend.service.ProductItemService;

@RestController
@RequestMapping("/api/v1/carts")
@CrossOrigin("*")
public class CartController {

	@Autowired
	private ProductItemService itemService;
	
	@Autowired
	private CartRepository cartRepository;

	@PostMapping
	public ResponseEntity<?> addCart(@RequestBody ProductItem itemProduct) throws URISyntaxException{
		ProductItem itemCreated = itemService.create(itemProduct);
		return ResponseEntity.created(ServletUriComponentsBuilder.fromPath("/{id}").buildAndExpand(itemCreated.getId()).toUri()).build();
	}
	
	@GetMapping
	public ResponseEntity<List<Cart>> getCarts(){
		return ResponseEntity.ok(cartRepository.findAll());
	}
}
