package br.com.mand.backend.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.mand.backend.model.ProductItem;

@Repository
public interface ProductItemRepository extends PagingAndSortingRepository<ProductItem, Integer>{

	@Query(value = "SELECT pi FROM ProductItem pi "
			+ " JOIN FETCH pi.product p"
			+ " JOIN FETCH pi.cart c"
			+ " WHERE p.id = ?1 AND c.id = ?2")
	public ProductItem existsByProductIdAndCartId(Integer productId, Integer cartId);
	
}
