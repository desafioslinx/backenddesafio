package br.com.mand.backend.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.mand.backend.model.Product;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>{

	@Query(value = "SELECT p FROM Product p WHERE p.purchaseCost IS NOT NULL")
	public List<Product> findAll();
	
}
