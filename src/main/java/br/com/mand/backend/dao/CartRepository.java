package br.com.mand.backend.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.mand.backend.model.Cart;

@Repository
public interface CartRepository extends PagingAndSortingRepository<Cart, Integer>{

	
	@Query(value = "SELECT c FROM Cart c LEFT JOIN FETCH ProductItem pi ON pi.cart.id = c.id ORDER BY c.id")
	public List<Cart> findAll();
	
}
